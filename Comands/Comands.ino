#include <Servo.h> //Include some librarry

//Variables 
bool   b = true; 		//1 byte
char   c = "a";			//1 byte
int	   i = 3;			//2 byte
long   l = 2147; 		//4 byte (max -> -2,147,483,647 to 2,147,483,647)
float  f = 1.4; 	    //4 byte (max -> -2,147,483,647 to 2,147,483,647)
double d = 2.12;		//4 byte (max -> -32,768 to 32,767)
string s = "a str";		//1 byte + x
array  a = ["a", 0];	//1 byte + x

//Used var
int ledPin = 13;   		
int digitalInPin = 7;   
int analogInPin = 1;
int delayPin = 2;
int delayVal = 0
int val = 0;       	
int digitalVal = 0;
int analogVal = 0;
unsigned long currentMillis;	//Create current time val
unsigned long startMillis;		//Create start time
unsigned long period = 1000;  	//the value is a number of milliseconds between changes

void setup() {
  pinMode(ledPin, OUTPUT);      // sets the digital pin 13 as output
  pinMode(inPin, INPUT);        // sets the digital pin 7 as input

  Serial.begin (9600); 			// Init console 

  startMillis = millis();  		//initial start time

}

void loop() {
  //Digital pins -> digitalRead('pin number') 
  digitalVal = digitalRead(digitalInPin);     // read the input pin
  digitalWrite(ledPin, DigitalVal);    		  // sets the LED to the button's value
  //digitalWrite('pin number', [HIGH, LOW] || [1, 0] || [true, false])

  //Analog pins -> analogRead('pin number') 
  analogVal = analogRead(analogInPin);     	 // read the input pin
  analogWrite(ledPin, analogVal);    		 // sets the LED to the button's value
  //analogWrite('pin number', [HIGH, LOW] || [1, 0] || [true, false])

  if(digitalVal){
  	Serial.println("Pin ");					 //Write information to console
  	Serial.print(digitalInPin);
  	Serial.print(" Value: ");
  	Serial.print(digitalVal);
  }

  if (analogVal){
  	Serial.println("Pin ");					 //Write information to console
  	Serial.print(analogInPin);
  	Serial.print(" Value: ");
  	Serial.print(analogVal);
  }

  delayVal = digitalRead(delayPin);
  if(delayVal){
  	currentMillis = millis();  					 //get the current time
  	if (currentMillis - startMillis >= period){  //test whether the period has elapsed
  		Serial.println("Delay 1sec ");
    	startMillis = currentMillis;  		     //IMPORTANT to save the start time of the current LED brightness
  	}
  }
}