// Include Libraries
#include "Arduino.h"
#include "OneButton.h"
#include <Servo.h>
#include <WiFi.h>
#include <dht.h>
dht DHT;

// Pin Definitions
#define WATER_POMP          1
#define DHT_PIN_DATA        0
#define LEDR_PIN_VIN        3
#define ACTIVE_TASK_BTN     2
#define SERVO_1             4
#define SERVO_2             5
#define WATER_CHECK_WARNING 12
#define WATER_CHECK_ERROR   13

//Castom val
bool danger = 0;    
bool active_task = 0;
int temperature = 0;
int humidity = 0;
OneButton button(ACTIVE_TASK_BTN, false);

//Server config
char* ssid     = "";
char* password = "";
WiFiServer server(80);
String header;

//Millis config
unsigned long day = 86400000;
unsigned long active_pomp_milis = 0;
unsigned long circuit_disabled_milis = 0;



void setup() {
    button.attachClick(active_task_circuit);
    Serial.print("Connecting to ");
    Serial.println(ssid);
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    // Print local IP address and start web server
    Serial.println("");
    Serial.println("WiFi connected.");
    Serial.println("IP address: ");
    Serial.print(WiFi.localIP());
    server.begin();
    Serial.begin (9600);
    //Set pin mod
    pinMode(WATER_POMP, OUTPUT);
    pinMode(DHT_PIN_DATA, INPUT);
    pinMode(LEDR_PIN_VIN, OUTPUT);
    pinMode(ACTIVE_TASK_BTN, INPUT);
    pinMode(SERVO_1, OUTPUT);
    pinMode(SERVO_2, INPUT);
    pinMode(WATER_CHECK_WARNING, INPUT);
    pinMode(WATER_CHECK_ERROR, INPUT);
}

void loop() {
    //Init
    button.tick();
    int check_DHT = DHT.read11(DHT11_PIN);
    temperature = DHT.temperature;
    humidity = DHT.humidity;
    soil_moisture_danger = digitalRead(SOILMOISTURE3V3_PIN_SIG);
    unsigned long currentMillis = millis();

    if(active_task){
        //Tasks
        pompActive(SERVO_1, 3000);
        pompActive(SERVO_2, 1000);

        //Controls
        danger(soil_moisture_danger);
        danger(!active_task);
    } else {
        circuit_disabled()
    }

}

void circuit_disabled(){
    if ((unsigned long)(currentMillis - circuit_disabled_milis) >= 1000) {
        digitalWrite(ACTIVE_TASK_BTN, HIGH);
        circuit_disabled_milis = millis();
    } else {
        digitalWrite(ACTIVE_TASK_BTN, LOW);
        circuit_disabled_milis = millis();  
    }
}

void active_task_circuit(){
    //On click close or open circuit
    active_task = !active_task;
}

void pompActive(pompNumber, work_time) {
    //Active pomp every tot period
    if ((unsigned long)(currentMillis - active_pomp_milis) >= interval) {
        pomp_working(pompNumber, work_time);
 	}
}

void pomp_working(pompNumber, work_time){
    //Activ pomp now and reset perio
    servo_working(pompNumber, "open");
    digitalWrite(WATER_POMP, true);
    delay(work_time);
    digitalWrite(WATER_POMP, false);
    servo_working(pompNumber, "close");
    active_pomp_milis = millis();
}

void servo_working(servo, position){
    //Open specific plant chanel
    myservo.attach(servo);
    if (position === "open") {
        for (pos = 0; pos <= 180; pos += 1) { 
            myservo.write(pos);             
            delay(15);                      
        }
    } else {
        for (pos = 180; pos >= 0; pos -= 1) { 
            myservo.write(pos);              
            delay(15);                      
        }
    }
    delay(1000);
}

void danger(check){
    //Active led if some problems
	if (check && danger == false) {
    	danger = true;
    } else {
    	danger = false;
    }
    digitalWrite(ACTIVE_TASK_BTN, danger);
}

void server(){
  WiFiClient client = server.available();  

  if (client) {                             
    Serial.println("New Client.");          
    String currentLine = "";               
    while (client.connected()) {           
      if (client.available()) {             
        char c = client.read();             
        Serial.write(c);                    
        header += c;
        if (c == '\n') {                    
          if (currentLine.length() == 0) {
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();
            
            if (header.indexOf("POST /plant/a") >= 0) {
                pomp_working(WATER_POMP_1, 3000)
            } else if (header.indexOf("POST /plant/b") >= 0) {
                pomp_working(WATER_POMP_2, 1000)
            } else if else if (header.indexOf("GET /info") >= 0) {
                client.println("{\"temperature\": \" " +  temperature +"\", \"humidity\": \" " +  humidity +"\"}");
            }

          } else { // if you got a newline, then clear currentLine
                currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
            currentLine += c;      // add it to the end of the currentLine
        }
      }
    }
    header = "";
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
}