#include "OneButton.h"

OneButton button(A1, false);

void setup() {

  button.attachClick(click);
  
  button.attachDoubleClick(doubleclick);
  
  button.attachPress(press);

  button.attachLongPressStart(longpressstart);

  button.attachLongPressStop(longpressend);

  button.isLongPressed(islongpressed)

  Serial.begin (9600);
} // setup
  
// main code here, to run repeatedly: 
void loop() {
  // keep watching the push button:
  button.tick();

  // You can implement other code in here or just wait a while 
  delay(10);
}

//click
void click(){
  Serial.println("click");
}
// doubleclick
void doubleclick() {
  Serial.println("double click");
} 
//press
void press(){
  Serial.println("press");
}
//longpressstart
void longpressstart(){
  Serial.println("long presss tart");
}
//longpressend
void longpressend(){
  Serial.println("long press end");
}
//islongpressed
void islongpressed(){
  Serial.println("is long pressed");  
}
// End