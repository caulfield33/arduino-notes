#include "OneButton.h"
#include <WiFi.h>

char* ssid     = "";
char* password = "";
WiFiServer server(80);
String header;

OneButton swithLigh(A1, false);
bool lightPosition = false;
int lightPowerPin = 2

void setup() {
  	swithLigh.attachClick(swithLightBtn);
  	pinMode(lightPowerPin, OUTPUT);
    alaloglWrite(lightPowerPin, LOW);

	Serial.print("Connecting to ");
	Serial.println(ssid);
	WiFi.begin(ssid, password);
	while (WiFi.status() != WL_CONNECTED) {
	delay(500);
	Serial.print(".");
	}
	// Print local IP address and start web server
	Serial.println("");
	Serial.println("WiFi connected.");
	Serial.println("IP address: ");
	Serial.print(WiFi.localIP());
	server.begin();
	//ip address/pc_light_swith
  Serial.begin (9600);
}

//Button light swith
void swithLightBtn(){
	lightPosition = !lightPosition;
	alaloglWrite(lightPowerPin, lightPosition)
}

void loop(){
  WiFiClient client = server.available();   // Listen for incoming clients

  if (client) {                             // If a new client connects,
    Serial.println("New Client.");          // print a message out in the serial port
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected()) {            // loop while the client's connected
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor
        header += c;
        if (c == '\n') {                    // if the byte is a newline character
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();
            
            if (header.indexOf("GET /pc_light_swith") >= 0) {
            	lightPosition = !lightPosition;
              	Serial.println("Light > ");
              	Serial.println(lightPosition);
              	alaloglWrite(lightPowerPin, lightPosition);
            }

          } else { // if you got a newline, then clear currentLine
            	currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
        	currentLine += c;      // add it to the end of the currentLine
        }
      }
    }
    header = "";
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
}